# Changelog

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/).



## [0.1.1] 2022 / 02 / 22

### Added
- Setup.cfg file
- Changelog file

### Changed

- __init__ file
- .gitlab_ci file

