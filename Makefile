init:
	pip install -r requirements.txt

test:
	pytest -v tests --cov-config=.coveragerc --cov=calcifer_pde

lint:
	pylint calcifer_pde

wheel:
	rm -rf build
	rm -rf dist
	python setup.py sdist bdist_wheel

upload_test:
	twine upload --repository-url https://test.pypi.org/legacy/ dist/*

upload:
	twine upload dist/*

.PHONY: init test lint
