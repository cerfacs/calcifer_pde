calcifer\_pde package
=====================

.. automodule:: calcifer_pde
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

calcifer\_pde.boundary module
-----------------------------

.. automodule:: calcifer_pde.boundary
   :members:
   :undoc-members:
   :show-inheritance:

calcifer\_pde.calcifer module
-----------------------------

.. automodule:: calcifer_pde.calcifer
   :members:
   :undoc-members:
   :show-inheritance:

calcifer\_pde.diff module
-------------------------

.. automodule:: calcifer_pde.diff
   :members:
   :undoc-members:
   :show-inheritance:

calcifer\_pde.domain module
---------------------------

.. automodule:: calcifer_pde.domain
   :members:
   :undoc-members:
   :show-inheritance:

calcifer\_pde.geometry module
-----------------------------

.. automodule:: calcifer_pde.geometry
   :members:
   :undoc-members:
   :show-inheritance:

