
Theory 
======

In Calcifer, we assume that the shell is thin enough to neglect the temperature gradient and thus heat conduction for the thickness-wise of the shell.

Now if we consider that the temperature in the shell is $T_h$ which is obviously higher than the temperature outside of the shell $T_c$,
the heat transfer from inside of the shell to outside

Therefore, the heat equation which describes the heat transfer around this shell can be written as below.

$$\alpha(\frac{\partial^{2} T}{\partial u^{2}} + \frac{\partial^{2} T}{\partial v^{2}}) + \frac{q_{V}}{\rho c_{p}}=0$$



