import numpy as np
import scipy as scp
from arnica.utils.nparray2xmf import NpArray2Xmf
from calcifer_pde.domain import Domain
from calcifer_pde import heat_solve
from calcifer_pde.geometry import Square, Donut, Donut_Local, Sphere, Staggered


def test_square(version):
    """Test the square Shapes"""
    if version == "basic":
        geo = Square(nx=80, ny=100, len_x=1.0, len_y=1.0, auto_metric=False)
    elif version == "diff1":
        geo = Square(nx=10, ny=10, len_x=1.0, len_y=1.0, auto_metric=True)
    elif version == "staggered":
        # Shows that autoatic differenciation does not work
        # on varable sampling meshes
        geo = Staggered(nx=80, ny=100, len_x=1.0, len_y=1.0)
    else:
        raise RuntimeError("version argument not implemented")

    dom = Domain(geo)

    dom.switch_bc_vmax_neuman(0.0)
    dom.switch_bc_vmin_neuman(0.0)
    # dom.switch_bc_vmin_dirichlet(100.)
    # dom.switch_bc_vmax_dirichlet(200.)
    # dom.switch_bc_umin_neuman(0.)
    # dom.switch_bc_umax_neuman(0.)
    dom.switch_bc_umin_dirichlet(200.0)
    dom.switch_bc_umax_dirichlet(100.0)
    # dom.switch_bc_v_perio()
    # dom.switch_bc_u_perio()

    rr = (dom.geo.x_coor - 0.3) ** 2 + (dom.geo.y_coor - 0.3) ** 2
    sigma = 0.0025
    gau = 1.0 / np.sqrt(2.0 * np.pi * sigma) * np.exp(-rr / (2.0 * sigma))
    lapl_scal_a = (
        1.0
        / np.sqrt(2.0 * np.pi * sigma)
        * (rr - 2.0 * sigma)
        / sigma ** 2.0
        * np.exp(-rr / (2.0 * sigma))
    )
    scal = np.zeros(dom.geo.shape)
    scal = np.where(np.sqrt(rr) < 1, gau, scal)

    sca = np.tile(
        np.linspace(0.0, 1.0, dom.geo.shape[0], endpoint=True), (dom.geo.shape[1], 1)
    ).T

    # LHS
    term = 300.0 + 300.0
    term /= -1e-3
    sh_to = dom.geo.shape[1] * dom.geo.shape[0]
    dum = np.zeros((sh_to, sh_to))
    dumay = np.copy(term)
    dumval = list(dumay.flatten())
    np.fill_diagonal(dum, dumval)
    dom.sterm_l = 0.0  # scp.sparse.csr_matrix(dum)

    # RHS
    sin_wave = np.tile(
        np.sin(np.linspace(0.0, 1.0, dom.geo.shape[1], endpoint=True) * np.pi) * 100.0
        + 800.0,
        dom.geo.shape[0],
    )
    sin_wave.flatten("F")
    s_term_r = 300.0 * (sin_wave + np.ones_like(sin_wave) * 800.0)
    s_term_r /= -1e-3
    dom.sterm_r = 0.0  # s_term_r

    g_x_scal = (dom.grad_x_csr * scal.ravel()).reshape(dom.shp2d)
    g_y_scal = (dom.grad_y_csr * scal.ravel()).reshape(dom.shp2d)
    lapl_scal = (dom.lapl * scal.ravel()).reshape(dom.shp2d)

    dom.laplacian_filter()

    init = 200.0 * np.ones(dom.geo.shape[0] * dom.geo.shape[1])
    sol_dirichlet = heat_solve(
        dom, init_field=init, k_coeff=22.0, sterm_r=None, sterm_l=None
    )

    print("Tmin, Tmax", sol_dirichlet.min(), sol_dirichlet.max())
    outh5 = NpArray2Xmf("out.h5")
    outh5.create_grid(dom.geo.x_coor, dom.geo.y_coor, dom.geo.z_coor)
    outh5.add_field(scal, "scal")
    outh5.add_field(g_x_scal, "grd_x_scal")
    outh5.add_field(g_y_scal, "grd_y_scal")
    outh5.add_field(lapl_scal, "lapl_scal")
    outh5.add_field(lapl_scal_a, "lap_scal_a")
    outh5.add_field(
        (lapl_scal - lapl_scal_a) / np.max(np.abs(lapl_scal) * 100.0), "err_scal"
    )
    outh5.add_field(sol_dirichlet, "sol_dirichlet")
    outh5.dump()


def test_donut(localframe=False):
    """test in the Donut shapes"""
    if not localframe:
        geo = Donut(
            nr=80,
            ntheta=100,
            r_min=1,
            r_max=2.0,
            theta_min=0,
            theta_max=0.5 * np.pi,
            auto_metric=True,
        )
    else:
        geo = Donut_Local(
            nr=80, ntheta=100, r_min=1, r_max=2.0, theta_min=0, theta_max=0.4 * np.pi
        )

    dom = Domain(geo)

    dom.switch_bc_umax_dirichlet(500.0)
    dom.switch_bc_umin_dirichlet(200.0)
    # dom.switch_bc_v_perio()

    dom.switch_bc_vmax_neuman(0.0)
    dom.switch_bc_vmin_neuman(0.0)

    rr = (dom.geo.x_coor - 1.0) ** 2 + (dom.geo.y_coor - 1.0) ** 2
    sigma = 0.0025
    gau = 1.0 / np.sqrt(2.0 * np.pi * sigma) * np.exp(-rr / (2.0 * sigma))
    lapl_scal_a = (
        1.0
        / np.sqrt(2.0 * np.pi * sigma)
        * (rr - 2.0 * sigma)
        / sigma ** 2.0
        * np.exp(-rr / (2.0 * sigma))
    )

    scal = np.zeros(dom.geo.shape)
    scal = np.where(np.sqrt(rr) < 1, gau, scal)

    g_x_scal = (dom.grad_x_csr * scal.ravel()).reshape(dom.shp2d)
    g_y_scal = (dom.grad_y_csr * scal.ravel()).reshape(dom.shp2d)
    lapl_scal = (dom.lapl * scal.ravel()).reshape(dom.shp2d)

    inv_r = scp.sparse.csr_matrix(
        (np.reciprocal(geo.radius).ravel(), (range(dom.shp1d), range(dom.shp1d))),
        shape=(dom.shp1d, dom.shp1d),
    )

    # compute laplacian with cylindrical contribution
    lapl2 = (
        dom.grad_x_csr.dot(dom.grad_x_csr)
        + inv_r * (dom.grad_x_csr)
        + dom.grad_y_csr.dot(dom.grad_y_csr)
    )
    dom.lapl = lapl2

    init = 200.0 * np.ones(dom.geo.shape[0] * dom.geo.shape[1])
    sol_dirichlet = heat_solve(dom, init_field=init, k_coeff=1.0)

    outh5 = NpArray2Xmf("out.h5")
    outh5.create_grid(dom.geo.x_coor, dom.geo.y_coor, dom.geo.z_coor)
    outh5.add_field(scal, "scal")
    outh5.add_field(g_x_scal, "grd_x_scal")
    outh5.add_field(g_y_scal, "grd_y_scal")
    outh5.add_field(lapl_scal, "lapl_scal")
    outh5.add_field(lapl_scal_a, "lap_scal_a")
    outh5.add_field(
        (lapl_scal - lapl_scal_a) / np.max(np.abs(lapl_scal) * 100.0), "err_scal"
    )
    outh5.add_field(sol_dirichlet, "sol_dirichlet")
    outh5.add_field(geo.analytic, "analytic")
    outh5.dump()


def test_sph():
    """ Thsest the speherical shape"""
    geo = Sphere(nphi=50, ntheta=100, rad=2)
    dom = Domain(geo)

    dom.switch_bc_vmax_neuman(0.0)
    dom.switch_bc_vmin_neuman(0.0)
    dom.switch_bc_umax_dirichlet(500.0)
    dom.switch_bc_umin_dirichlet(200.0)
    # dom.switch_bc_v_perio()

    rad_0 = (
        (dom.geo.x_coor - 0.0) ** 2
        + (dom.geo.y_coor - 2.0) ** 2
        + (dom.geo.z_coor - 0.0) ** 2
    )
    angle = 0.9 * np.pi / 2
    rad_1 = (
        (dom.geo.x_coor - 2.0 * np.cos(angle)) ** 2
        + (dom.geo.y_coor - 0) ** 2
        + (dom.geo.z_coor - 2.0 * np.sin(angle)) ** 2
    )

    sigma = 0.05
    gau1 = 1.0 / np.sqrt(2.0 * np.pi * sigma) * np.exp(-rad_0 / (2.0 * sigma))
    lapl_a_scal = (
        1.0
        / np.sqrt(2.0 * np.pi * sigma)
        * (rad_0 - 2.0 * sigma)
        / sigma ** 2.0
        * np.exp(-rad_0 / (2.0 * sigma))
    )

    gau2 = 1.0 / np.sqrt(2.0 * np.pi * sigma) * np.exp(-rad_1 / (2.0 * sigma))
    lapl_a_scal += (
        1.0
        / np.sqrt(2.0 * np.pi * sigma)
        * (rad_1 - 2.0 * sigma)
        / sigma ** 2.0
        * np.exp(-rad_1 / (2.0 * sigma))
    )

    scal = np.zeros(dom.shp2d)
    scal = np.where(np.sqrt(rad_0) < 1, gau1 + gau2, scal)

    # Test
    # LHS
    term = 300.0 + 300.0
    term /= -1e-3
    sh_to = dom.geo.shape[1] * dom.geo.shape[0]
    dum = np.zeros((sh_to, sh_to))
    dumay = np.copy(term)
    dumval = list(dumay.flatten())
    np.fill_diagonal(dum, dumval)
    dom.sterm_l = scp.sparse.csr_matrix(dum)

    # RHS
    sin_wave_ori = np.tile(
        np.sin(np.linspace(0.0, 1.0, dom.geo.shape[0], endpoint=True) * 2.0 * np.pi)
        * 100.0
        + 800.0,
        (dom.geo.shape[1], 1),
    ).T
    sin_wave = sin_wave_ori.ravel()
    s_term_r = 300.0 * (sin_wave + np.ones_like(sin_wave) * 800.0)
    s_term_r /= -1e-3
    dom.sterm_r = s_term_r

    # laplacian calculated inside
    lapl_scal = (dom.lapl * scal.ravel()).reshape(dom.shp2d)

    # compute laplacian with cylindrical contribution
    g_x_scal = (dom.grad_x_csr * scal.ravel()).reshape(dom.shp2d)
    g_y_scal = (dom.grad_y_csr * scal.ravel()).reshape(dom.shp2d)

    inv_r = scp.sparse.csr_matrix(
        (
            (np.reciprocal(geo.radius) * dom.geo.dr / dom.geo.du).ravel(),
            (range(dom.shp1d), range(dom.shp1d)),
        ),
        shape=(dom.shp1d, dom.shp1d),
    )

    lapl2 = (
        dom.grad_x_csr.dot(dom.grad_x_csr)
        + inv_r * (dom.grad_x_csr)
        + dom.grad_y_csr.dot(dom.grad_y_csr)
    )

    lapl_r_scal = (lapl2 * scal.ravel()).reshape(dom.shp2d)
    dom.lapl = lapl2
    # dom.laplacian_filter()
    # dom.lapl = dom.filter2.dot(dom.lapl)
    lapl_rf_scal = (dom.lapl * scal.ravel()).reshape(dom.shp2d)

    init = 500.0 * np.ones(dom.geo.shape[0] * dom.geo.shape[1])
    sol_dirichlet = heat_solve(
        dom, init_field=init, k_coeff=1.0, sterm_r=None, sterm_l=None
    )

    outh5 = NpArray2Xmf("out.h5")
    outh5.create_grid(dom.geo.x_coor, dom.geo.y_coor, dom.geo.z_coor)
    outh5.add_field(scal, "scal")
    outh5.add_field(g_x_scal, "grd_x")
    outh5.add_field(g_y_scal, "grd_y")
    outh5.add_field(lapl_scal, "lapl")
    outh5.add_field(lapl_r_scal, "lapl_r")
    outh5.add_field(lapl_rf_scal, "lapl_rf")
    outh5.add_field(lapl_a_scal, "lap_a")
    outh5.add_field(
        (lapl_scal - lapl_a_scal) / np.max(np.abs(lapl_scal) * 100.0), "err"
    )
    outh5.add_field(geo.radius, "radius")
    outh5.add_field(geo.dr, "dr")
    outh5.add_field(geo.dtheta, "dt")
    outh5.add_field(geo.rdtheta, "rdt")
    outh5.add_field(sol_dirichlet, "sol_dirichlet")
    outh5.add_field(geo.analytic, "analytic")
    outh5.add_field(sin_wave.reshape(dom.shp2d), "sin_wave")
    outh5.dump()


if __name__ == "__main__":
    test_sph()
    # test_square(version='diff1')
    # test_donut(localframe=True)
