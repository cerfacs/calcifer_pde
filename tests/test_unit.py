import calcifer_pde.calcifer as calf
import calcifer_pde.diff as diff
import numpy as np


def test_diff_first_dim():

    inarray = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
    outarray = np.array([[3, 3, 3], [3, 3, 3], [3, 3, 3]])

    assert np.array_equal(outarray, diff.diff_first_dim(inarray))


def test_diff_scnd_dim():

    inarray = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
    outarray = np.array([[3, 3, 3], [3, 3, 3], [3, 3, 3]])

    assert np.array_equal(outarray, diff.diff_first_dim(inarray))
